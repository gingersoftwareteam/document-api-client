export class DocumentCorrectionRequest{
    apiKey: string;
    lang: string;
    generateRecommendations: boolean;
    generateSynonyms: boolean;
    capitalization: boolean;
    text: string;
    contentType: string;
}