import {TextFragment} from "./text-fragment";

export class Sentence {
    text: string;
    sentenceFragments: TextFragment[];
    sentenceInfo: any;

    constructor(sentenceInfo: any, text: string, sentenceFragments: TextFragment[]) {
        this.text = text;
        this.sentenceFragments = sentenceFragments;
        this.sentenceInfo = sentenceInfo;
    }
}