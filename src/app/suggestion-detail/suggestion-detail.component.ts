import { Component, OnInit, Input } from '@angular/core';
import { CorrectionCategory } from "../correction-category";

@Component({
  selector: 'app-suggestion-detail',
  templateUrl: './suggestion-detail.component.html',
  styleUrls: ['./suggestion-detail.component.css']
})
export class SuggestionDetailComponent implements OnInit {

  @Input() suggestion: any;
  @Input() correction: any;

  isCollapsed: boolean;

  constructor() { }

  ngOnInit() {
    this.isCollapsed = true;
  }

  toCategory(categoryId: number){
      return CorrectionCategory[categoryId];
  }

  getText(): string {
    if(this.suggestion.Text === "")
      return this.correction.MistakeText;

    return this.suggestion.Text;
  }

  markAsRemoved() : boolean {
    return this.suggestion.Text === "";
  }
}
