export enum CorrectionType {
    Spelling = 1,
    Misused = 2,
    Grammar = 3,
    Synonyms = 4,
    Recommendation = 5,
    Punctuation = 6
}