import { Component, OnInit } from '@angular/core';
import { DocumentCorrectionRequest} from "../document-correction-request";
import { DocumentService} from "../document.service";
import { Router } from "@angular/router";
import { DataService } from "../data.service";
import { HtmlFileService } from "../html-file.service";

declare var $: any;

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.css']
})
export class RequestFormComponent implements OnInit {

  documentCorrectionRequest: DocumentCorrectionRequest;
  errorMessage: string;
  errorType: string;
  correcting: boolean;
  loadingHtmlFromUrl: boolean;
  htmlDocumentFileName: string;
  htmlDocumentUrl: string;
  prevProd: boolean;

  constructor(private documentService: DocumentService,
              private router: Router,
              private dataService: DataService,
              private htmlFileService: HtmlFileService) { }

  ngOnInit() {
      this.documentCorrectionRequest = this.dataService.documentCorrectionRequest;
      this.htmlDocumentFileName = this.dataService.htmlDocumentFileName;
      this.htmlDocumentUrl = this.dataService.htmlDocumentUrl;
      this.prevProd = false;
  }

  onSubmit(){
      this.correcting = true;
      let startTime = new Date().getTime();
      this.documentService.correctDocument(this.documentCorrectionRequest, this.prevProd)
          .subscribe(response => {
              this.dataService.executionTime = new Date().getTime() - startTime;
              this.dataService.documentCorrectionResponse = response;
              this.dataService.htmlDocumentFileName = this.htmlDocumentFileName;
              this.dataService.htmlDocumentUrl = this.htmlDocumentUrl;
              this.router.navigate(['/summary']);
              this.correcting = false;
          }, err => {
              this.errorType = err.error.ErrorType || "Error";
              this.errorMessage = err.error.Message || err.message;
              this.showErrorModal();
              this.correcting = false;
          });
  }

  showErrorModal(){
      $('#errorModal').modal('show');
  }

  clear(){
      this.documentCorrectionRequest.text = '';
  }

  loadHtmlDocumentFromUrl(url: string){
      if(url){
          let proxyUrl = 'https://cors-anywhere.herokuapp.com/';
          this.documentCorrectionRequest.text = '';
          this.loadingHtmlFromUrl = true;
          this.htmlFileService.loadFromUrl(proxyUrl + url).subscribe(result=>{
              this.documentCorrectionRequest.text = result;
              this.loadingHtmlFromUrl = false;
          }, err=>{
              this.errorType = "Error";
              this.errorMessage = err.message.replace(proxyUrl, '');
              this.loadingHtmlFromUrl = false;
              this.showErrorModal();
          });
      }
  }

  onFileChange(files: FileList) {
      if(files[0]){
          this.htmlFileService.loadFromFile(files[0]).subscribe(text=>{
              this.documentCorrectionRequest.text = text;
              this.htmlDocumentFileName = files[0].name;
          },err=>{
              this.errorType = err.error.ErrorType || "Error";
              this.errorMessage = err.error.Message || err.message;
              this.showErrorModal();
          });
      }
    }
}
