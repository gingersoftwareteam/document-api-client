import {Segment} from "./segment";
import {CorrectionsAndRecommendationsCombiner} from "./corrections-and-recommendations-combiner";
import {TextFragmentGenerator} from "./text-fragment-generator";

export class SegmentGenerator{

    private correctionsAndRecommendationsCombiner: CorrectionsAndRecommendationsCombiner;
    private textFragmentGenerator: TextFragmentGenerator;

    constructor(){
        this.correctionsAndRecommendationsCombiner = new CorrectionsAndRecommendationsCombiner();
        this.textFragmentGenerator = new TextFragmentGenerator();
    }


    generateSegments(text:string, documentCorrectionResponse: any): Segment[]{
        let segments: Segment[] = [];

        if(!documentCorrectionResponse.GingerTheDocumentResult.Segments)
            return segments;

        for(let [index, segment] of documentCorrectionResponse.GingerTheDocumentResult.Segments.entries()){
            let combinedCorrections = this.correctionsAndRecommendationsCombiner.combineCorrectionsAndRecommendations(segment.FromIndex,
                segment.ToIndex,
                documentCorrectionResponse.GingerTheDocumentResult.Corrections,
                documentCorrectionResponse.GingerTheDocumentResult.Recommendations);

                let textFragments = this.textFragmentGenerator.generateTextFragments(text, segment.FromIndex, segment.ToIndex, combinedCorrections);
                segments.push(new Segment(index, segment, text.substring(segment.FromIndex, segment.ToIndex + 1), textFragments, combinedCorrections));

        }

        return segments;
    }
}