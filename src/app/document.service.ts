import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { DocumentCorrectionRequest} from "./document-correction-request";
import { catchError } from "rxjs/internal/operators";
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  private documentApiBaseUrl = environment.documentApiBaseUrl;

  constructor(private http: HttpClient) { }

  correctDocument (request: DocumentCorrectionRequest, prevProd: boolean): Observable<any>{

    let httpParams = new HttpParams()
        .set('apiKey', request.apiKey)
        .set('lang', request.lang)
        .set('generateRecommendations', request.generateRecommendations.toString())
        .set('generateSynonyms', request.generateSynonyms.toString())
        .set('avoidCapitalization', (!request.capitalization).toString())
        .set('withSummary', "true")
        .set('withSegments', "true");

    let httpHeaders = new HttpHeaders()
        .set('Content-Type', request.contentType);

    let baseUrl = prevProd ? "https://prevprod.gingersoftware.com/" : this.documentApiBaseUrl;

    return this.http.post(baseUrl + "correction/v1/document", encodeURIComponent(request.text) ,{headers: httpHeaders, params: httpParams})
        .pipe(
            catchError(this.handleError('correctDocument'))
        );
  }

  private handleError<T> (operation = 'operation') {
      return (error: any): Observable<T> => {

          // TODO: send the error to remote logging infrastructure
          console.error(error); // log to console instead

          // TODO: better job of transforming error for user consumption
          console.log(`${operation} failed: ${error.message}`);

          // Let the app keep running by returning an empty result.
          return throwError(error);
      };
  }
}
