import {Component, Input, OnInit} from '@angular/core';
import {Confidence} from "../confidence";
import {CorrectionType} from "../correction-type";
import {CorrectionCategory} from "../correction-category";

@Component({
  selector: 'app-correction-item',
  templateUrl: './correction-item.component.html',
  styleUrls: ['./correction-item.component.css']
})
export class CorrectionItemComponent implements OnInit {

  @Input() correction: any;

  constructor() { }

  ngOnInit() {
  }

  toConfidence(confidence: number){
      return Confidence[confidence];
  }

  toCorrectionType(correctionType: number){
      return CorrectionType[correctionType];
  }

  toCategory(categoryId: number){
      return CorrectionCategory[categoryId];
  }

  getStyleForCorrection(correction){
      return{
          'spelling': correction.CorrectionType === CorrectionType.Spelling,
          'grammar': correction.CorrectionType === CorrectionType.Grammar,
          'misused': correction.CorrectionType === CorrectionType.Misused,
          'punctuation': correction.CorrectionType === CorrectionType.Punctuation,
          'synonym': correction.CorrectionType === CorrectionType.Synonyms,
          'recommendation': correction.CorrectionType === CorrectionType.Recommendation
      };
  }

}
