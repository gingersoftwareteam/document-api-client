import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sentence-detail',
  templateUrl: './sentence-detail.component.html',
  styleUrls: ['./sentence-detail.component.css']
})
export class SentenceDetailComponent implements OnInit {

  @Input() sentenceInfo: any;

  constructor() { }

  ngOnInit() {
  }

}
