import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrectionDetailComponent } from './correction-detail.component';

describe('CorrectionDetailComponent', () => {
  let component: CorrectionDetailComponent;
  let fixture: ComponentFixture<CorrectionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorrectionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrectionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
