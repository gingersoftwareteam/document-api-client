import { Component, OnInit, Input } from '@angular/core';
import { Confidence } from "../confidence";
import { CorrectionType } from "../correction-type";
import { CorrectionCategory } from "../correction-category";

@Component({
  selector: 'app-correction-detail',
  templateUrl: './correction-detail.component.html',
  styleUrls: ['./correction-detail.component.css']
})
export class CorrectionDetailComponent implements OnInit {

  @Input() correction: any;
  @Input() additionalDetails: boolean;

  constructor() { }

  ngOnInit() {
  }

  toConfidence(confidence: number){
    return Confidence[confidence];
  }

  toCorrectionType(correctionType: number){
    return CorrectionType[correctionType];
  }

  toCategory(categoryId: number){
    return CorrectionCategory[categoryId];
  }
}
