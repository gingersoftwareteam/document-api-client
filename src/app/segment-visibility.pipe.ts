import { Pipe, PipeTransform } from '@angular/core';
import {Segment} from "./segment";

@Pipe({
  name: 'segmentVisibility'
})
export class SegmentVisibilityPipe implements PipeTransform {

  transform(segments: Segment[], showAllSegments: boolean): any {
     if(showAllSegments === undefined || showAllSegments === true)
       return segments;

     return segments.filter(function(segment){
        return segment.combinedCorrections.length > 0;
     });
  }

}
