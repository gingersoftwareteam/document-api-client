import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-summary-detail',
  templateUrl: './summary-detail.component.html',
  styleUrls: ['./summary-detail.component.css']
})
export class SummaryDetailComponent implements OnInit {

  @Input() summary: any;
  @Input() documentType: string;

  constructor() { }

  ngOnInit() {
  }
}
