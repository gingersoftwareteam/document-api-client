import { Injectable } from '@angular/core';
import { DocumentCorrectionRequest } from "./document-correction-request";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  documentCorrectionRequest: DocumentCorrectionRequest = {
      apiKey: '',
      lang: 'indifferent',
      generateRecommendations: false,
      generateSynonyms: false,
      capitalization: true,
      text: '',
      contentType: 'text/plain'
  };

  documentCorrectionResponse: any;
  executionTime: number;
  htmlDocumentFileName: string;
  htmlDocumentUrl: string;

  constructor() { }
}
