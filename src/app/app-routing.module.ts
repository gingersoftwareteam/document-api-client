import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestFormComponent } from './request-form/request-form.component'
import { SummaryComponent } from "./summary/summary.component";

const routes: Routes = [
    //{path: 'correct', component: RequestFormComponent},
    //{path: '', redirectTo: '/correct', pathMatch: 'full'},
    {path: '', component: RequestFormComponent},
    {path: 'summary', component: SummaryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
