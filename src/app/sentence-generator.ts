import {Sentence} from "./sentence";
import {CorrectionsAndRecommendationsCombiner} from "./corrections-and-recommendations-combiner";
import {TextFragmentGenerator} from "./text-fragment-generator";

export class SentenceGenerator{

    private correctionsAndRecommendationsCombiner: CorrectionsAndRecommendationsCombiner;
    private textFragmentGenerator: TextFragmentGenerator;

    constructor(){
        this.correctionsAndRecommendationsCombiner = new CorrectionsAndRecommendationsCombiner();
        this.textFragmentGenerator = new TextFragmentGenerator();
    }

    generateSentences(text:string, documentCorrectionResponse: any): Sentence[]{
        let sentences: Sentence[] = [];

        for(let sentence of documentCorrectionResponse.GingerTheDocumentResult.Sentences){
            let combinedCorrections = this.correctionsAndRecommendationsCombiner.combineCorrectionsAndRecommendations(sentence.FromIndex,
                                                                                                                      sentence.ToIndex,
                                                                                                                      documentCorrectionResponse.GingerTheDocumentResult.Corrections,
                                                                                                                      documentCorrectionResponse.GingerTheDocumentResult.Recommendations);
            let textFragments = this.textFragmentGenerator.generateTextFragments(text, sentence.FromIndex, sentence.ToIndex, combinedCorrections);
            sentences.push(new Sentence(sentence, text.substring(sentence.FromIndex, sentence.ToIndex + 1), textFragments));
        }

        return sentences;
    }
}