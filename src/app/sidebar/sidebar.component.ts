import { Component, OnInit, Input } from '@angular/core';
import { TextFragment } from "../text-fragment";
import { Sentence } from "../sentence";
import { Segment } from "../segment";
import {CorrectionType} from "../correction-type";

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

    @Input() textFragment: TextFragment;
    @Input() sentence: Sentence;
    @Input() segment: Segment;
    @Input() summary: any;
    @Input() documentType: string;

    constructor() { }

    ngOnInit() {
    }

    getStyleForCorrection(correction){

        return{
            'spelling': correction.CorrectionType === CorrectionType.Spelling,
            'grammar': correction.CorrectionType === CorrectionType.Grammar,
            'misused': correction.CorrectionType === CorrectionType.Misused,
            'punctuation': correction.CorrectionType === CorrectionType.Punctuation,
            'synonym': correction.CorrectionType === CorrectionType.Synonyms
        };
    }
}