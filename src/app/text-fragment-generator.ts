import {TextFragment} from "./text-fragment";
import {CorrectionType} from "./correction-type";

export class TextFragmentGenerator {

    generateTextFragments(text: string, fromIndex: number, toIndex: number, combinedCorrections: any[]): TextFragment[]{

        let textFragments: TextFragment[] = [];

        //If no corrections - return one fragment
        if(combinedCorrections.length === 0){
            textFragments.push(new TextFragment(text.substring(fromIndex, toIndex + 1), fromIndex, toIndex, null, null, null, null));
            return textFragments;
        }

        //From beginning of sentence to the first correction
        if(combinedCorrections[0].From > 0)
            textFragments.push(new TextFragment(text.substring(fromIndex, combinedCorrections[0].From),fromIndex, combinedCorrections[0].From - 1 ,null, null, null,null));

        //Iterate over corrections
        for(let i = 0; i < combinedCorrections.length; i++){
            textFragments = textFragments.concat(this.generateTextFragmentsFromCombinedCorrection(text, combinedCorrections[i]));
            if(i < combinedCorrections.length - 1){
                textFragments.push(new TextFragment(text.substring(combinedCorrections[i].To + 1, combinedCorrections[i+1].From),combinedCorrections[i].To + 1, combinedCorrections[i+1].From - 1,null, null,null,null));
            }
        }

        //From last correction to end of sentence
        if(combinedCorrections[combinedCorrections.length-1].To < toIndex)
            textFragments.push(new TextFragment(text.substring(combinedCorrections[combinedCorrections.length-1].To + 1, toIndex + 1), combinedCorrections[combinedCorrections.length-1].To + 1, toIndex, null, null, null, null));

        return textFragments;
    }

    private generateTextFragmentsFromCombinedCorrection(text: string, combinedCorrection: any): TextFragment[]{
        let textFragments: TextFragment[] = [];
        let indices = [];

        for(let i = 0; i < combinedCorrection.Corrections.length; i++){
            if(!indices.find(ind=>ind.index === combinedCorrection.Corrections[i].From))
                indices.push({index: combinedCorrection.Corrections[i].From, isFrom: true});
            if(!indices.find(ind=>ind.index === combinedCorrection.Corrections[i].To))
                indices.push({index: combinedCorrection.Corrections[i].To, isFrom: false});
        }

        if(indices.length === 1){

            let correction = combinedCorrection.Corrections[0].CorrectionType !== CorrectionType.Recommendation ? combinedCorrection.Corrections[0] : null;
            let fullCorrectionText = correction ? text.substring(correction.From, correction.To + 1) : null;
            let recommendation = combinedCorrection.Corrections[0].CorrectionType === CorrectionType.Recommendation ? combinedCorrection.Corrections[0] : null;
            let fullRecommendationText = recommendation ? text.substring(recommendation.From, recommendation.To + 1) : null;

            textFragments.push(new TextFragment(text.substring(indices[0].index, indices[0].index + 1), indices[0].index, indices[0].index, correction, recommendation, fullCorrectionText, fullRecommendationText));
            return textFragments;
        }

        indices.sort((a,b)=>a.index-b.index);

        for(let i = 0; i < indices.length - 1; i++){
            let fromIndex = indices[i].isFrom ? indices[i].index : indices[i].index + 1;
            let toIndex = indices[i+1].isFrom ? indices[i+1].index : indices[i+1].index + 1;
            let correction = combinedCorrection.Corrections.find(c=>c.CorrectionType !== CorrectionType.Recommendation && fromIndex <= c.To && c.From <= toIndex - 1);
            let fullCorrectionText = correction ? text.substring(correction.From, correction.To + 1) : null;
            let recommendation = combinedCorrection.Corrections.find(r=>r.CorrectionType === CorrectionType.Recommendation && fromIndex <= r.To && r.From <= toIndex - 1);
            let fullRecommendationText = recommendation ? text.substring(recommendation.From, recommendation.To + 1) : null;

            let textFragment = new TextFragment(text.substring(fromIndex, toIndex), fromIndex, toIndex, correction, recommendation, fullCorrectionText, fullRecommendationText);
            textFragments.push(textFragment);
        }
        return textFragments;
    }
}
