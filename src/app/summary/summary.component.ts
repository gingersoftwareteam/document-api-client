import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { DataService } from "../data.service";
import { SentenceGenerator } from "../sentence-generator";
import { SegmentGenerator } from "../segment-generator";
import { Sentence } from "../sentence";
import { Segment } from "../segment";
import { TextFragment } from "../text-fragment";
import { CorrectionType } from "../correction-type";

declare var $: any;

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})

export class SummaryComponent implements OnInit {

  text: string;
  documentCorrectionResponse: any;
  sentences: Sentence[];
  segments: Segment[];
  selectedTextFragment: TextFragment;
  selectedSentence: Sentence;
  selectedSegment: Segment;
  summary: any;
  executionTime: number;
  documentType: string;
  showAllSegments: boolean;
  autoReplace: boolean;

  private sentenceGenerator: SentenceGenerator;
  private segmentGenerator: SegmentGenerator;

  constructor(private location: Location,
              private dataService: DataService) {
      this.sentenceGenerator = new SentenceGenerator();
      this.segmentGenerator = new SegmentGenerator();
      this.showAllSegments = false;
      this.autoReplace = false;
  }

  ngOnInit() {
    this.text = this.dataService.documentCorrectionRequest.text;
    this.executionTime = this.dataService.executionTime;
    this.documentCorrectionResponse = this.dataService.documentCorrectionResponse;
    this.documentType = this.dataService.documentCorrectionRequest.contentType === 'text/plain' ? 'Text' : 'HTML';
    if(this.documentType === 'Text'){
        this.sentences = this.sentenceGenerator.generateSentences(this.text, this.documentCorrectionResponse);
    } else if(this.documentType === 'HTML'){
        this.segments = this.segmentGenerator.generateSegments(this.text, this.documentCorrectionResponse);
    }

    this.summary = this.documentCorrectionResponse.GingerTheDocumentResult.Summary;

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
  }

  onSelectTextFragment(textFragment){
    this.selectedTextFragment = textFragment;
    this.selectedSentence = null;
    this.selectedSegment = null;
  }

  onSelectSentence(sentence){
      this.selectedSentence = sentence;
      this.selectedTextFragment = null;
  }

  onSelectSegment(segment){
      this.selectedSegment = segment;
      this.selectedTextFragment = null;
  }

  goBack(){
    this.location.back();
  }

  getStyleForTextFragment(textFragment){
    let disableColor = this.autoReplace && textFragment.canReplaceTextWithSuggestion();
    let shouldRemoveWord = textFragment.shouldRemove();
    let collapse = this.autoReplace && shouldRemoveWord;
    return{
      'correction': textFragment.correction,
      'spelling': textFragment.correction && textFragment.correction.CorrectionType === CorrectionType.Spelling && !disableColor,
      'grammar': textFragment.correction && textFragment.correction.CorrectionType === CorrectionType.Grammar && !disableColor,
      'misused': textFragment.correction && textFragment.correction.CorrectionType === CorrectionType.Misused && !disableColor,
      'punctuation': textFragment.correction && textFragment.correction.CorrectionType === CorrectionType.Punctuation && !disableColor,
      'synonym': textFragment.correction && textFragment.correction.CorrectionType === CorrectionType.Synonyms,
      'recommendation': textFragment.recommendation,
      'border border-dark': (textFragment.correction || textFragment.recommendation) && textFragment === this.selectedTextFragment,
      'removed-word': shouldRemoveWord,
      'collapse': collapse
    };
  }
}