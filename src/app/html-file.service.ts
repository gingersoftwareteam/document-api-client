import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HtmlFileService {

  constructor(private http: HttpClient) { }

  loadFromFile(file: Blob): Observable<any> {
      let reader = new FileReader();
      return new Observable(function (observer) {
          reader.onload = () => {
              observer.next(reader.result.toString());
              observer.complete();
          };
          reader.readAsText(file);
      });
  }

  loadFromUrl(url:string): Observable<any>{
      return this.http.get(url, {responseType: 'text'});
  }
}
