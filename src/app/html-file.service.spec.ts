import { TestBed } from '@angular/core/testing';

import { HtmlFileService } from './html-file.service';

describe('HtmlFileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HtmlFileService = TestBed.get(HtmlFileService);
    expect(service).toBeTruthy();
  });
});
