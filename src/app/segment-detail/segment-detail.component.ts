import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-segment-detail',
  templateUrl: './segment-detail.component.html',
  styleUrls: ['./segment-detail.component.css']
})
export class SegmentDetailComponent implements OnInit {

  @Input() segmentInfo: any;

  constructor() { }

  ngOnInit() {
  }

}
