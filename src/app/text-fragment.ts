import { CorrectionCategory } from "./correction-category";

export class TextFragment {
    text: string;
    from: number;
    to: number;
    correction: any;
    recommendation: any;
    fullCorrectionText: string;
    fullRecommendationText: string;

    constructor(text: string, from: number, to: number, correction: any, recommendation: any, fullCorrectionText: string, fullRecommendationText: string) {
        this.text = text;
        this.from = from;
        this.to = to;
        this.correction = correction;
        this.recommendation = recommendation;
        this.fullCorrectionText = fullCorrectionText;
        this.fullRecommendationText = fullRecommendationText;
    }

    getText(replaceSuggestion: boolean): string{
        if(replaceSuggestion && this.canReplaceTextWithSuggestion())
            return this.correction.Suggestions[0].Text;

        return this.text;
    }

    canReplaceTextWithSuggestion(): boolean{
        return this.correction && this.correction.ShouldReplace && this.correction.Suggestions && this.correction.Suggestions.length > 0;
    }

    shouldRemove(): boolean{
        return this.correction && this.correction.Suggestions && this.correction.Suggestions.length > 0 && this.correction.Suggestions[0].Text === "";
    }
}