import {TextFragment} from "./text-fragment";

export class Segment {
    text: string;
    segmentFragments: TextFragment[];
    segmentInfo: any;
    index: number;
    combinedCorrections: any[];

    constructor(index: number, segmentInfo: any, text: string, segmentFragments: TextFragment[], combinedCorrections: any[]) {
        this.text = text;
        this.segmentFragments = segmentFragments;
        this.segmentInfo = segmentInfo;
        this.index = index;
        this.combinedCorrections = combinedCorrections;
    }
}