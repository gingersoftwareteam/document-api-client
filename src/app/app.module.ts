import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule }    from '@angular/common/http';
import { RequestFormComponent } from './request-form/request-form.component';
import { SummaryComponent } from './summary/summary.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CorrectionDetailComponent } from './correction-detail/correction-detail.component';
import { SentenceDetailComponent } from './sentence-detail/sentence-detail.component';
import { SuggestionDetailComponent } from './suggestion-detail/suggestion-detail.component';
import { SummaryDetailComponent } from './summary-detail/summary-detail.component';
import { NoCorrectionsPipe } from './no-corrections.pipe';
import { CorrectionItemComponent } from './correction-item/correction-item.component';
import { SegmentDetailComponent } from './segment-detail/segment-detail.component';
import { SegmentVisibilityPipe } from './segment-visibility.pipe';

@NgModule({
  declarations: [
    AppComponent,
    RequestFormComponent,
    SummaryComponent,
    SidebarComponent,
    CorrectionDetailComponent,
    SentenceDetailComponent,
    SuggestionDetailComponent,
    SummaryDetailComponent,
    NoCorrectionsPipe,
    CorrectionItemComponent,
    SegmentDetailComponent,
    SegmentVisibilityPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
