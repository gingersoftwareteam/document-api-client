export class CorrectionsAndRecommendationsCombiner {


    combineCorrectionsAndRecommendations(fromIndex: number, toIndex: number, corrections: any[], recommendations: any[]) : any[]{
        let correctionsForSentence = this.getDataForSentence(fromIndex, toIndex, corrections);
        let recommendationsForSentence = this.getDataForSentence(fromIndex, toIndex, recommendations);
        return this.combine(correctionsForSentence, recommendationsForSentence);
    }


    private getDataForSentence(fromIndex: number, toIndex: number, corrections: any[]): any[] {
        let correctionsForSentence = [];

        if(!corrections)
            return correctionsForSentence;

        for(let i = 0; i < corrections.length; i++){
            if(corrections[i].From >= fromIndex && corrections[i].To <= toIndex)
                correctionsForSentence.push(corrections[i]);
        }
        return correctionsForSentence;
    }

    private combine(corrections: any[], recommendations: any[]): any[] {
        let correctionsAndRecommendations = [];
        let sorted = [...corrections, ...recommendations];
        sorted.sort((a,b)=>(a.From-b.From));

        let i = 0;
        while(i < sorted.length) {
            let j = i + 1;
            let combined = {
                Corrections: [sorted[i]],
                From: sorted[i].From,
                To: sorted[i].To
            };
            while(j < sorted.length && this.shouldCombine(combined, sorted[j])){
                combined.Corrections.push(sorted[j]);
                if(sorted[j].To > combined.To)
                    combined.To = sorted[j].To;
                j++;
            }
            correctionsAndRecommendations.push(combined);
            i = j;
        }

        return correctionsAndRecommendations;
    }

    private shouldCombine(combined: any, correction: any): boolean{
        return ((combined.To >= correction.From && combined.To <= correction.To) ||
            (correction.From >= combined.From && correction.To <= combined.To));
    }
}
