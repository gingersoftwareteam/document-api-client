import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noCorrections'
})
export class NoCorrectionsPipe implements PipeTransform {

  transform(correctionsByType: any[]): any[] {
    return correctionsByType.filter(correctionType=>correctionType.Value > 0);
  }

}
